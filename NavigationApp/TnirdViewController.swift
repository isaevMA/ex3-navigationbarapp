//
//  TnirdViewController.swift
//  NavigationApp
//
//  Created by Maksim Isaev on 26.02.2020.
//  Copyright © 2020 Maksim Isaev. All rights reserved.
//

import UIKit

class TnirdViewController: UIViewController {

    @IBOutlet var textField: UITextField!
    
    var text: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.text = text
    }
    
    @IBAction func closeThirdVC() {
        text = textField.text
    }
    
}
