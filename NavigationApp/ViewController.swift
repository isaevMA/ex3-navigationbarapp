//
//  ViewController.swift
//  NavigationApp
//
//  Created by Maksim Isaev on 26.02.2020.
//  Copyright © 2020 Maksim Isaev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    // Передаем значение title при переходе
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let secondVC = segue.destination
        secondVC.navigationItem.title = segue.identifier
    }


}

