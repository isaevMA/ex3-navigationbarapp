//
//  SecondViewController.swift
//  NavigationApp
//
//  Created by Maksim Isaev on 26.02.2020.
//  Copyright © 2020 Maksim Isaev. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "edit" {
            let thirdVC = segue.destination as! TnirdViewController
            thirdVC.text = segue.identifier
            
        }
    }
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {
        let thirdVC = segue.source as! TnirdViewController
        title = thirdVC.text
    }
    

}
